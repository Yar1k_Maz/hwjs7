let tabs = document.querySelector(".tabs");
let tabsContent = document.querySelector(".tabs-content");
tabs.addEventListener("click", (e) => {
   let target = e.target
  tabs.querySelector(".active")?.classList.toggle("active");
  if (target.closest(".tabs-title")) {
    target.classList.toggle("active");
  }
  let datId = target.dataset.tab;
  tabsContent
    .querySelector(".active-content")
    ?.classList.toggle("active-content");
  tabsContent
    .querySelector(`.content[data-tab="${datId}"]`)
    ?.classList.toggle("active-content");
});
